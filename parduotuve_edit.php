<?php 
require "parduotuve_functions.php";
$prekiumasyvas = isdb();
$preke = null;
if (isset($_GET['redaguoti'])) {
    foreach ($prekiumasyvas as $prek) {
        if ($_GET['redaguoti'] == $prek->ID) {
            $preke = $prek;
            break;
        }
    }
}
if (isset($_GET['preke']) && isset($_GET['kaina']) && $preke != null) {
    redb($preke->ID, trim($_GET['preke']), trim ($_GET['kaina']));
    header("Location: ./administracinis_sarasas.php");
}
?>

<?php if (isset($_GET['redaguoti']) && isset($prekiumasyvas[$_GET['redaguoti']])): $redirect=false; ?>
<form>
    <input  type='hidden' 
            value='<?php echo $_GET['redaguoti']; ?>' 
            name='redaguoti' />
    <input  type='text' 
            value='<?php echo $preke->preke; ?>'
            name='preke'/>
    <input  type='number' step="0.01"
            value='<?php echo $preke->kaina / 100; ?>'
            name='kaina'/>


    <input  type='submit' />
</form>
<?php endif; 


?>