<?php
function isfailo ()
{
    $filename = "./prekes.json";
        if (is_readable($filename))
        {
            $filecontents = file_get_contents("./prekes.json");
            $json_data = json_decode($filecontents,true);
            $prekiumasyvas = array();

            if (!empty($json_data))
            {
                foreach ($json_data as $i => $e):
                    array_push($prekiumasyvas, ["id" => $i, "preke" => $e["preke"], "kaina" => $e["kaina"]]);
                endforeach; 
            }
        }
        else
        {
            echo "Prekių failo neįmanoma nuskaityti.";
        }       
        return $prekiumasyvas;
}

function get_db() {
    try {
        $pdo = new PDO('mysql:host=localhost', 'root', '');
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $pdo;
    } catch (PDOException $e) {
        echo $e;
        return 'Error';
    }
}

function isdb() {
    $pdo = get_db();
    $pdo->prepare('USE parduotuve')->execute();
    $prekes = $pdo->prepare('SELECT * FROM prekes;');
    $prekes->execute();
    return $prekes->fetchAll(PDO::FETCH_CLASS);
}

function irduombaze($preke, $kaina) {
    $pdo = get_db();
    $pdo->prepare('USE parduotuve')->execute();
    $kaina = $kaina * 100;
    $uzklausa = $pdo->prepare('INSERT INTO prekes (preke, kaina) VALUES (:preke, :kaina);');
    $uzklausa->bindParam(':preke', $preke);
    $uzklausa->bindParam(':kaina', $kaina);
    $uzklausa->execute();
}

function isduombaze($id) {
    $pdo = get_db();
    $pdo->prepare('USE parduotuve')->execute();
    $uzklausa = $pdo->prepare('DELETE FROM prekes WHERE ID=:id');
    $uzklausa->bindParam(':id', $id);
    if ($uzklausa->execute()) {
        return true;
    } else {
        return false;
    }
}
 function reduombaze($id, $preke, $kaina) {
    $kaina = $kaina * 100;
    $pdo = get_db();
    $pdo->prepare('USE parduotuve')->execute();
    $uzklausa = $pdo->prepare('UPDATE prekes SET preke=:preke, kaina=:kaina WHERE ID=:id');
    $uzklausa->bindParam(':preke', $preke);
    $uzklausa->bindParam(':kaina', $kaina);
    $uzklausa->bindParam(':id', $id);
    if ($uzklausa->execute()) {
        return true;
    }
    return false;
 }