<?php
session_start();

require "parduotuve_functions.php";
$prekiumasyvas = isdb();

if (!isset($_SESSION["wishlist"])) {
    $_SESSION["wishlist"] = array();
}
if (!is_array($_SESSION["wishlist"]))
{
    $_SESSION["wishlist"] = array();
}

if (isset($_GET['add'])) {
    $prideti = null;
    foreach ($prekiumasyvas as $preke) {
        if ($preke->ID == $_GET['add']) {
            $prideti = $preke;
            break;
        }
    }
    if ($prideti != null) {
        $duplicate = false;
        foreach ($_SESSION["wishlist"] as $w) {
            if ($w['id'] == $prideti->ID) {
                $duplicate = true;
            }
        }
        if (!$duplicate) {
            if ($prideti !== null) {
                array_push(
                $_SESSION["wishlist"],
                [
                    "id" => $prideti->ID,
                    "preke" => $prideti->preke,
                    "kaina" => $prideti->kaina,
                ]
            );
            }
        }
    }
}

if (isset($_GET['delete']) && isset($_SESSION['wishlist'][$_GET['delete']]) )
{
    unset($_SESSION['wishlist'][$_GET['delete']]);
}
?>

<html>
    <body>
    <?php 
        
        if (!empty($prekiumasyvas))
        { ?>
            <table border="1">
            <tr>
                <th></th>
                <th>Preke</th>
                <th>Kaina</th>
                <th>Wishlist</th>
            </tr>

            <?php foreach ($prekiumasyvas as $e):?>
                <tr>
                    <td> <?php echo $e->ID; ?></th>
                    <td> <?php echo $e->preke; ?></td>
                    <td> <?php echo $e->kaina; ?></td>
                    <td> <a href=<?php echo "\"?add=" .$e->ID . "\""; ?>> <center><3</center> </td>
                </tr>
            <?php endforeach; 
        }
        else echo "Sandėlyje prekių nėra";
        ?>
        </table>
        <hr>

        <h2> Wishlist </h2>
        <?php if (!empty($_SESSION["wishlist"]))
        { 
            ?>
            <table border="1">
            <tr>
                <th></th>
                <th>Preke</th>
                <th>Kaina</th>
                <th>Istrinti</th>
            </tr>
            <?php foreach ($_SESSION["wishlist"] as $w => $e):?>
            <?php ?>
                <tr>
                    <td> <?php echo $w+1; ?></th>
                    <td> <?php echo $e["preke"]; ?></td>
                    <td> <?php echo $e["kaina"]; ?></td>
                    <td> <a href=<?php echo "\"?delete=" .$w . "\""; ?>> <center> X </center> </td>
                </tr>
            <?php ?>
            <?php ?>
            <?php endforeach; ?>


            </table>
        <?php
        }
        else echo "Norų sąrašas yra tuščias"; ?>

    <body>
</html>