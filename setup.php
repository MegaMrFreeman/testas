<?php
try {
    $pdo = new PDO('mysql:host=localhost', 'root', ''); #localhost arba 127.0.0.1
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo $e;
}
$uzklausa = $pdo->prepare('SHOW DATABASES');
$uzklausa->execute();

$atsakymas = $uzklausa->fetchAll(PDO::FETCH_CLASS);

$db_egzistuoja = false;
$db_sukurta = false;

foreach($atsakymas as $ats){
    if ($ats->Database == 'parduotuve') {
        $db_egzistuoja = true;
    break;
    }
}
if (!$db_egzistuoja) {
    $db_sukurta = $pdo->prepare('CREATE DATABASE parduotuve;')->execute();
}
if ($db_sukurta || $db_egzistuoja) {
    $pdo->prepare('USE parduotuve;')->execute();

    //lenteliu rodymas
    $lenteles = $pdo->prepare('SHOW TABLES');
    $lenteles->execute();
    $atsakymas = $lenteles->fetchAll(PDO::FETCH_CLASS);
    print_r($atsakymas);


    if($pdo->prepare('CREATE TABLE prekes (ID int NOT NULL AUTO_INCREMENT, preke varchar(120), kaina int, PRIMARY KEY(ID));')->execute()) {
        echo 'Sekmingai sukurta lentele';
    }
}
